/* eslint semi: [1, always] */
/* eslint indent: [1, 2] */
/* eslint no-var: 0 */

var
  reference = 'default',
  vendor = 'defr',
  theme = 'ftl',
  resources = `addons/${reference}/${vendor}/${theme}-theme/resources/`,
  Elixir = require('laravel-elixir');

require('laravel-elixir-webpack-official');
require('laravel-elixir-vue-2');

Elixir.config.sourcemaps = true;

Elixir.webpack.mergeConfig({
  babel: {
    presets: ['es2015', 'stage-2'],
    plugins: ['transform-runtime'],
  },
  resolve: {
    alias: {
      vue: 'vue/dist/vue.js',
      jquery: 'jquery/src/jquery',
      $: 'jquery/src/jquery',
    }
  },
  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: 'style!css',
      },
      {
        test: /\.(sc|ass)$/,
        includePaths: [
          './node_modules/bootstrap-sass/assets/stylesheets'
        ],
        loader: 'style!css!sass',
      },
      {
        test: /\.styl(us)?$/,
        loader: 'style!css!stylus',
      },
      {
        test: /\.(png|jpe?g|gif|ico)$/,
        loader: 'file?name=/img/[name].[ext]',
      },
      {
        test: /\.(ttf|eot|svg|woff2?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file?name=/fonts/[name].[ext]',
      },
    ],
  },
});

Elixir(mix => {
  mix
    .copy(`${resources}fonts`, 'public/fonts')
    // .copy(`${resources}img`, 'public/img')
    .stylus(`./${resources}stylus/main.styl`, 'public/css', `${resources}stylus`)
    // .sass('theme.scss', 'public/css', `${resources}scss`)
    .webpack('main.js', 'public/js', `${resources}js`);
});
