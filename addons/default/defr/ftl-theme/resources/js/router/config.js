import Vue from 'vue'
import VueRouter from 'vue-router'
import Meta from 'vue-meta'
import routes from './routes'

Vue.use(Meta)
Vue.use(VueRouter)

const router = new VueRouter({
  routes,
  linkActiveClass: 'active',
  mode: 'history',
  // scrollBehavior (to, from, savedPosition) {
  //   if (to.hash) {
  //     return { selector: to.hash }
  //   }
  //   if (savedPosition) {
  //     return savedPosition
  //   }
  //   return { selector: '.bg-crumbs' }
  // }
})

/**
 * Before a route is resolved we check for
 * the token if the route is marked as
 * requireAuth.
 */
// router.beforeEach(beforeEach)

export default router
