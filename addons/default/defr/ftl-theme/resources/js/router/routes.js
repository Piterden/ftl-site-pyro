import Vue from 'vue'

import IndexPage from '../pages/IndexPage.vue'
import PortfolioPage from '../pages/PortfolioPage.vue'

/**
 * Router config
 *
 * @param  {String}   mode
 * @param  {String}   linkActiveClass
 * @param  {Array}    routes
 */
export default [{
    name: 'home.index',
    path: '/',
    meta: {
        name: 'Главная'
    },
    component (resolve) {
        return resolve(Vue.component('index-page', IndexPage))
    }
}, {
    name: 'portfolio.index',
    path: '/portfolio',
    meta: {
        name: 'Портфолио'
    },
    component (resolve) {
        return resolve(Vue.component('portfolio-page', PortfolioPage))
    }
}]
