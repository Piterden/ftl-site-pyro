import $ from 'jquery'

import Vue from 'vue'
import http from './plugins/http'
import VueWaypoint from 'vue-waypoint'
import VueParticles from 'vue-particles'
import eventbus from './plugins/eventbus'
import collection from './plugins/collection'


import App from './App.vue'
import router from './router'

// import Flickity from 'vue-flickity'
// import VueChartist from 'v-chartist'
// import Icon from 'vue-awesome/components/Icon'

window.$ = window.jQuery = $

Vue.use(http)
Vue.use(eventbus)
Vue.use(collection)
Vue.use(VueWaypoint)
Vue.use(VueParticles)

new Vue({
  components: { App },
  router,
  el: '#App',
})
