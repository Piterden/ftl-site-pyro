<?php namespace Defr\FtlTheme;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;

class FtlThemeServiceProvider extends AddonServiceProvider
{

    /**
     * Addon bindings
     *
     * @var array
     */
    protected $bindings = [
        // LinkFormBuilder::class => \Defr\FtlTheme\Link\Form\LinkFormBuilder::class,
    ];
}
