<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrThemeFtlAddFieldsToNavigation extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* @var StreamInterface $stream */
        $stream = $this->streams()->findBySlugAndNamespace('links', 'navigation');

        /* @var FieldInterface $field */
        $field = $this->fields()->create([
            'namespace' => 'navigation',
            'slug'      => 'icon',
            'type'      => 'anomaly.field_type.file',
            'locked'    => 1,
            'config'    => [
                'folder' => 1,
            ],
        ]);

        /* @var AssignmentInterface */
        $this->assignments()->create([
            'stream' => $stream,
            'field'  => $field,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /* @var StreamInterface $stream */
        if (!$stream = $this->streams()->findBySlugAndNamespace('links', 'navigation'))
        {
            return;
        }

        /* @var FieldInterface $field */
        if (!$field = $this->fields()->findBySlugAndNamespace('icon', 'navigation'))
        {
            return;
        }

        /* @var AssignmentInterface $assignment */
        if (!$assignment = $this->assignments()->findByStreamAndField($stream, $field))
        {
            return;
        }

        $this->assignments()->delete($assignment);
        $this->fields()->delete($field);
    }
}
